"use strict";

const {sendToMaster} = require("./bot");

module.exports = exports = function error(err)
{
	let msg = "Une erreur est survenue ; lisez la console pour plus de détails.";

	if(err && err.message)
	{
		const {message} = err;
		if(message === "read ECONNRESET" || [403, 404, 502, 503, 522].includes(err.httpStatus))
			return;

		msg += err.name === "DiscordAPIError" ? `\nMessage : ${message}\nChemin : ${err.path}` : `\nMessage : ${message}`;
	}

	sendToMaster(msg, console.error);
	console.error(err);
}
