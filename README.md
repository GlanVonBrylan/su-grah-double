
# Suh Grah Double

This bot is french so if you don't speak french... well, too bad!

Version actuelle : 1.0

# Presentation
Réactions type "delete this", hugu etc.

# Utilisation
Ce bot est à utiliser avec Node.JS (≥14) avec le paquet `discord.js` v12.

`auth.json`
```JSON
{
	"token": "le token d'authentification de votre bot",
 	"master": "votre id d'utilisateurice",
	"prefix": "préfixe"
}
```

## Exemple de commande

`commands/hug.json`
```JSON
{
	"aliases": ["hugu", "calin", "câlin"],
	"help": "Câlin !",
	"text": "%AUTHOR% câline %RECIPIENT% !`",
	"pics": ["https://media.giphy.com/media/42YlR8u9gV5Cw/giphy.gif"]
}
```


# Licence
**Su Grah Double** est fourni sous Licence Publique Rien À Branler (WTFPL). Pour plus de détails, lisez COPYING.txt, ou ce lien : [http://sam.zoy.org/lprab/COPYING](http://www.wtfpl.net/txt/copying)

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)
