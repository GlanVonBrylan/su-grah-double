"use strict";

const Discord = require("discord.js");
const INTENTS = Discord.Intents.FLAGS;
const auth = require("./auth.json");
const {prefix} = auth;
if(!prefix)
	throw new Error("I need a prefix!");
const pLength = prefix.length;

const client = new Discord.Client({
	intents: new Discord.Intents([
		INTENTS.GUILDS,
		INTENTS.GUILD_MESSAGES,
		INTENTS.DIRECT_MESSAGES,
	]),
	partials: ["CHANNEL"], // for DMs
	presence: {activities: [{name: "$aide"}] , status: "online"},
});

var master, myself;

exports.sendToMaster = (msg, onError = error) => master.send(msg).catch(onError);
exports.master = () => master;
exports.myself = () => client.user;

const error = require("./error");
const commands = require("./commands");
const manage = require("./commands/manage");

client.login(auth.token);

client.on("ready", () => {
	myself = client.user;
	client.users.fetch(auth.master).then(m => master = m);
	console.log(`Connecté en tant que ${myself.tag} !`);
});



client.on("messageCreate", async msg => {
	if(msg.author.bot || !msg.content.startsWith(prefix) || !msg.guild && msg.author !== master)
		return;

	const cmd = msg.content.split(" ", 1)[0].substring(pLength).toLowerCase();
	if(!cmd)
		return;

	if(cmd === "help" || cmd === "aide")
	{
		msg.channel.send({ embeds: [{
			title: "Aide",
			description: `Préfixe : ${prefix}`,
			fields: commands.help,
		}]}).catch(error);
	}
	else if(cmd === "manage")
		manage(msg);
	else
		commands(cmd, msg);
});
