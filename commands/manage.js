"use strict";

const error = require("../error");
const {master} = require("../bot");
const {info, commands, help, reload} = require(".");
const {readFile, writeFile, unlink} = require("fs").promises;
const {existsSync} = require("fs");

const reservedNames = ["help", "manage"];

const urlRegexp = /^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(\#[-a-z\d_]*)?$/i;

function isVideo(url)
{ // No dot before webm, it's normal, webm is already 4 characters
	return [".avi", ".mp4", "webm", ".mov", ".mkv"].includes(url.substring(url.length - 4).toLowerCase());
}

module.exports = exports = msg => {
	if(msg.author !== master())
		return;

	const {channel} = msg;
	let [, cmd, ...args] = msg.content.split(" ");

	cmd = cmd?.toLowerCase();

	if(!cmd || cmd === "help")
	{
		channel.send({embeds: [{
			title: "Gestion des commandes",
			fields: [
				{ name: "list", value: "Liste les réactions existantes." },
				{ name: "add <nom>//[alias1 alias2 alias3]//<help text>//<text>", value: "Ajoute une nouvelle réaction. Exemple : `$manage add hug//hugu calin câlin//Câline quelqu'un//%AUTHOR% câline %RECIPIENT% ! >///<`\n`%AUTHOR%` sera remplacé par le nom de la personne ayant fait la commande, et `%RECIPIENT%` par celui de la personne mentionnée.\nLa nouvelle réaction commencera sans images, ajoutez-en avec `addPic`.\nS'il ne faut pas mettre d'alias, laissez vide. Exemple : `$manage add hug////Câline quelqu'un//etc`" },
				{ name: "addPic <nom> <urls>", value: "Ajoute les images indiquées à la réaction donnée. `<nom>` doit être le nom principal et non un alias.\nExemple : `addPic hug http://pictures.com/hug1.gif http://pictures.com/hug2.gif http://pictures.com/hug3.gif`" },
				{ name: "removePic <nom> <urls>", value: "Retire les images indiquées à la réaction. "},
				{ name: "edit <nom> <attr> <val>", value: "Modifie un des attributs de la réaction parmis `aliases`, `help` et `text`.\nExemple : `$manage edit hug aliases hugu calin câlin snuggle`" },
				{ name: "get <nom>", value: "Récupère le fichier JSON décrivant la commande." },
				{ name: "delete <nom>", value: "Supprime définitivement la réaction donnée." },
			],
		}]}).catch(error);
	}
	else switch(cmd)
	{
	case "list":
		channel.send({embeds: [{
			fields: Object.entries(info).map(([name, {aliases}]) => ({name, value: aliases ? `Alias : ${aliases.join(", ")}` : "*Aucun alias*", inline: true})),
		}]}).catch(error);
		break;

	case "add": {
		let [name, aliases, help, ...text] = args.join(" ").split("//").map(a => a.trim());
		name = name.toLowerCase();
		if(reservedNames.includes(name))
			return channel.send("Ce nom est réservé.").catch(error);
		if(name in info)
			return channel.send("Ce nom est déjà pris.").catch(error);

		const command = { help, text: text.join("//"), pics: [] };

		if(aliases) for(const alias of command.aliases = aliases.toLowerCase().split(" ").map(a => a.trim()).filter(a => a && a !== name))
		{
			if(reservedNames.includes(alias))
				return channel.send("Cet alias est réservé.").catch(error);
			if(alias in commands)
				return channel.send(`L'alias "${alias}" est déjà pris.`).catch(error);
		}

		writeFile(`${__dirname}/${name}.json`, JSON.stringify(command)).then(err => {
			if(err) error(err);
			else {
				reload(name);
				msg.react("✅").catch(error);
			}
		});
		} break;

	case "addpic":
		changePics(msg, args, "add");
		break;

	case "removepic":
		changePics(msg, args, "remove");
		break;

	case "edit": {
		const [name, attr, ...val] = args;
		const command = info[name];
		if(!command)
			return channel.send("Cette réaction n'existe pas.").catch(error);

		switch(attr)
		{
		case "aliases":
			command.aliases = val.map(a => a.trim());
			break;

		case "help":
		case "text":
			command[attr] = val.join(" ").trim();
			break;

		default:
			return channel.send("Attribut incorrect.").catch(error);
		}
		msg.react("✅").catch(error);
		} break;

	case "get":
		const name = args[0];
		channel.send({files: [{attachment: `${__dirname}/${name}.json`}]}).catch(error);
		break;

	case "remove":
	case "delete": {
		const name = args[0];
		const fileName = `${__dirname}/${name}.json`;
		if(!existsSync(fileName))
			channel.send("Cette command n'existe pas.").catch(error);
		else
		{
			unlink(fileName).then(err => {
				if(err) error(err);
				else
				{
					const command = commands[name];
					help.splice(command.helpId, 1);
					delete info[name];
					delete commands[name];
					for(const alias of command.aliases || [])
						delete commands[alias];

					msg.react("✅").catch(error);
				}
			});
		}
		} break;
	}
}


function changePics(msg, [name, ...urls], mode = "add")
{
	const command = info[name];
	if(!command)
		return msg.channel.send("Cette réaction n'existe pas.").catch(error);

	urls = urls.map(url => ul.trim());
	const {pics} = command;
	if(mode === "add") for(const url of urls)
	{
		if(pics.includes(url))
			continue;
		if(!urlRegexp.test(url))
			return msg.channel.send("Ce n'est pas une URL valide.").catch(error);
		if(isVideo(url))
			return msg.channel.send("Malheureusement, je ne peux pas gérer les vidéos...").catch(error);

		pics.push(url);
	}
	else for(const url of urls)
	{
		const index = pics.indexOf(url);
		if(index !== -1)
			pics.splice(index, 1);
	}

	writeFile(`${__dirname}/${name}.json`, JSON.stringify({
		aliases: command.aliases,
		help: command.help,
		text: command.text,
		pics,
	})).then(err => {
		if(err) error(err);
		else msg.react("✅").catch(error);
	})
}
