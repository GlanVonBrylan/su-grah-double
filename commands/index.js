"use strict";

const error = require("../error");
const { myself } = require("../bot");

Array.prototype.rand = function() {
	return this[~~(Math.random() * this.length)];
}

const AUTHOR = "%AUTHOR%", RECIPIENT = "%RECIPIENT%";

module.exports = exports = (cmd, msg) => {
	const command = commands[cmd];

	if(!command)
		return msg.react("❔").then(msg.guild ? reaction => setTimeout(() => reaction.users.remove(myself()).catch(Function()), 2000) : Function(), error);

	const {member, mentions: {members}} = msg;
	if(command.requiresMention && !members.size)
		return msg.channel.send("Il faut mentionner quelqu’un !").catch(error);

	msg.channel.send({embeds: [{
		image: {url: command.pics.rand()},
		author: {
			name: command.text.replace(AUTHOR, member.displayName)
			.replace(RECIPIENT, members.size > 1 ? "plein de gens" : members.first()?.displayName),
			icon_url: msg.author.avatarURL(),
		},
	}]}).catch(error);
}


exports.reload = name => {
	const current = commands[name];
	if(current)
	{
		exports.help.splice(current.helpId, 1);
		//delete exports.info[name];
		//delete commands[name];
		for(const alias of command.aliases || [])
			delete commands[alias];
	}

	load(name+".json");
}


///// LOAD ALL COMMANDS /////

exports.help = [];
exports.info = {};
const commands = exports.commands = {};
const {readFileSync} = require("fs");

require("fs").readdirSync(__dirname).forEach(load);

function load(cmd)
{
	if(!cmd.endsWith(".json"))
		return;

	const command = JSON.parse(readFileSync(`${__dirname}/${cmd}`));
	if(command.text.includes(RECIPIENT))
		command.requiresMention = true;

	const name = cmd.substring(0, cmd.length - 5);
	command.helpId = exports.help.push({
		name: command.requiresMention ? `${name} @mention` : name,
		value: `${command.help}${command.aliases ? `\n*Alias : \`${command.aliases.join("`, `")}\`*` : ""}`,
		inline: true,
	})-1;

	exports.info[name] = command;

	for(const alias of (command.aliases || []).concat(name))
		commands[alias] = command;
}
